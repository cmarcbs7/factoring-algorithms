# Integer Factorization
Prime factorization algorithms from [Wikipedia](https://en.wikipedia.org/wiki/Integer_factorization):

- Special Purpose
  - Trial Division
- General Purpose
  - _None yet_
- Quantum
  - _None yet_

## License
Dual-licensed to be compatible with the Rust project.

This software and associated documentation files are licensed under one of the
following:

- [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0)
- [MIT License](https://opensource.org/licenses/MIT)

at your option. This file may not be copied, modified, or distributed except
according to those terms.

