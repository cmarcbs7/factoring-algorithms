//! Integer factorization algorithms

use primes::PrimeSet;

//////////////////////////////////////////////////////////////////////////////
// Special-Purpose Algorithms
//////////////////////////////////////////////////////////////////////////////

/// Standard trial division
pub fn trial_division(factor_target: u64) -> Vec<u64> {
    let mut prime_factors = Vec::new();

    if factor_target != 0 {
        prime_factors.push(1);

        let mut last_value = 2;
        let mut remainder = factor_target;

        while remainder > 1 {
            if remainder % last_value == 0 {
                prime_factors.push(last_value);

                while remainder % last_value == 0 {
                    remainder /= last_value;
                }
            }

            last_value += 1;
        }
    }

    prime_factors
}

/// Trial division, only testing prime numbers
pub fn prime_division(factor_target: u64) -> Vec<u64> {
    let mut prime_factors = Vec::new();

    if factor_target != 0 {
        prime_factors.push(1);

        let mut all_primes = PrimeSet::new();
        let mut prime_iter = all_primes.iter();
        let mut remainder = factor_target;

        while remainder > 1 {
            let prime = prime_iter.next();

            match prime {
                Some(next_prime) => {
                    if remainder % next_prime == 0 {
                        prime_factors.push(next_prime);

                        while remainder % next_prime == 0 {
                            remainder /= next_prime;
                        }
                    }
                }
                _ => panic!("ran out of prime numbers"),
            }
        }
    }

    prime_factors
}

/// Wheel factorization
pub fn wheel(factor_target: u64) -> Vec<u64> {
    unimplemented!();
}

/// Pollard's rho algorithm
pub fn rho(factor_target: u64) -> Vec<u64> {
    unimplemented!();
}

/// Fermat's factorization method
pub fn fermat(factor_target: u64) -> Vec<u64> {
    unimplemented!();
}

/// Fermat's factorization method
pub fn euler(factor_target: u64) -> Vec<u64> {
    unimplemented!();
}

//////////////////////////////////////////////////////////////////////////////
// General-Purpose Algorithms
//////////////////////////////////////////////////////////////////////////////

/// Dixon's algorithm
pub fn dixon(n: u64) -> Vec<u64> {
    unimplemented!();
}

/// Quadratic sieve
pub fn qsieve(n: u64) -> Vec<u64> {
    unimplemented!();
}

/// Rational sieve
pub fn rsieve(n: u64) -> Vec<u64> {
    unimplemented!();
}

//////////////////////////////////////////////////////////////////////////////
// Additional Algorithms
//////////////////////////////////////////////////////////////////////////////

pub fn shor(n: u64) -> Vec<u64> {
    unimplemented!();
}

//////////////////////////////////////////////////////////////////////////////
// Unit Tests
//////////////////////////////////////////////////////////////////////////////

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_trial_division() {
        let a = 0;
        assert_eq!(0, trial_division(a).len());

        let b = 1;
        assert_eq!(vec![1], trial_division(b));

        let c = 2;
        assert_eq!(vec![1, 2], trial_division(c));

        let d = 3;
        assert_eq!(vec![1, 3], trial_division(d));

        let e = 12;
        assert_eq!(vec![1, 2, 3], trial_division(e));

        let f = 11;
        assert_eq!(vec![1, 11], trial_division(f));

        let g = 119;
        assert_eq!(vec![1, 7, 17], trial_division(g));

        let h = 256;
        assert_eq!(vec![1, 2], trial_division(h));
    }

    #[test]
    fn test_prime_division() {
        let a = 0;
        assert_eq!(0, prime_division(a).len());

        let b = 1;
        assert_eq!(vec![1], prime_division(b));

        let c = 2;
        assert_eq!(vec![1, 2], prime_division(c));

        let d = 3;
        assert_eq!(vec![1, 3], prime_division(d));

        let e = 12;
        assert_eq!(vec![1, 2, 3], prime_division(e));

        let f = 11;
        assert_eq!(vec![1, 11], prime_division(f));

        let g = 119;
        assert_eq!(vec![1, 7, 17], prime_division(g));

        let h = 256;
        assert_eq!(vec![1, 2], prime_division(h));
    }
}
