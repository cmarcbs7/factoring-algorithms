use rust_factoring_algorithms;

fn main() {
    let ans = rust_factoring_algorithms::prime_trial_division(600851475143);

    println!("{:?}", ans.last());
}